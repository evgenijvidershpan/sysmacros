@echo off
SET PATH=%PATH%;%PAD%\media\ffmpeg\bin

IF ":%~1" EQU ":" (
    echo usage: %~0 {video} {audio} {output}
)

IF not exist "%~1" exit /b 1
IF not exist "%~2" exit /b 1
IF exist "%~3" exit /b 1

ffmpeg.exe -i "%~1" -i "%~2" -map 0:v:0 -map 1:a:0 -codec:v copy -codec:a copy -movflags +faststart "%~3"

