@ECHO OFF
setlocal enabledelayedexpansion
SET HKEY=HKCU\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers

IF "x%~1" EQU "x" (
    echo usage: %~n0 c:\dir\app.exe [/d^|RUNASINVOKER^|RUNASADMIN] && echo 
    echo ----------------------------- Current items list: -----------------------------
    REG QUERY "%HKEY%" /f *
    EXIT /b 1
)
IF "%~2" EQU "/d" (
    REG DELETE "%HKEY%" /v "%~1"
    EXIT /b 0
)
IF NOT EXIST "%~1" (
    ECHO file "%~1" not exist!
    EXIT /b 2
)

SET APP=%~f1
SET STARTUP=%~2

IF "x%STARTUP%" NEQ "x" GOTO check_start_type
:select_start_type
    ECHO Select startup type:
    ECHO    1. RUNASINVOKER
    ECHO    2. RUNASADMIN
    SET /P "_I=: " || GOTO select_start_type
    IF "!_I!" EQU "1" SET STARTUP=RUNASINVOKER && GOTO apply_start_type
    IF "!_I!" EQU "2" SET STARTUP=RUNASADMIN && GOTO apply_start_type
    ECHO Invalid idx [!_I!] try again...
    GOTO select_start_type

:check_start_type
    IF "!STARTUP!" EQU "RUNASINVOKER" GOTO apply_start_type
    IF "!STARTUP!" EQU "RUNASADMIN" GOTO apply_start_type
    ECHO unsupported startup type: [%STARTUP%]
    GOTO select_start_type

:apply_start_type
    SET STARTUP=%STARTUP: =%
    ECHO Set startup type [%STARTUP%] to "%APP%"
    REG ADD "%HKEY%" /v "%APP%" /t REG_SZ /d "%STARTUP%" /f

:eof
