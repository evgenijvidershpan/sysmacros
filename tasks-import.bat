@echo off

if "x%~1" == "x" (
    echo shell task import utility v 1.0
    echo usage: %~0 *.xml
    exit /b 0
)

for %%i in (%~1) do call :Import "%%i"
goto eof

:Import
    schtasks /create /TN "%~n1" /XML "%~1"
    exit /b 0
:eof

