@echo off
IF "%~1x" == "x" goto :usage
set key=%~1

set key=%key:HKLM\=HKEY_LOCAL_MACHINE\%
set key=%key:HKCU\=HKEY_CURRENT_USER\%
set key=%key:HKCR\=HKEY_CLASSES_ROOT\%
set key=%key:HKU\=HKEY_USERS\%
set key=%key:HKCC\=HKEY_CURRENT_CONFIG\%

echo jump to [%key%]

reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Applets\Regedit" /v "LastKey" /t REG_SZ /d "%key%" /f
start regedit
exit /b 0

:usage
echo USAGE: %~n0 "HKCU\SOFTWARE"
