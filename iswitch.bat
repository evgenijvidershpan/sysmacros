@setlocal enabledelayedexpansion
@if ":%~2%~3%~4%~5%~6%~7%~8%~9" == ":" exit /b 1
@set CA=%~1
@set CA=echo %CA: =^&echo %
@set "CASE="
@for /F "tokens=* usebackq" %%i in (`cmd.exe /c "(%CA%) | findstr /I /L "%~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9""`) do @set "CASE=!CASE! %%i"
@if ":!CASE!" == ":" exit /b 2
@set CASE=%CASE:  = %
@set CASE=%CASE:~1,-1%
@endlocal & set "CASE=%CASE%"
@exit /b 0
