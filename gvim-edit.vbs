Set xapp = CreateObject("Shell.Application")
Set xshl = CreateObject("WScript.Shell")

cmd = """+call MyActivate()"""
For Each arg In Wscript.Arguments
   cmd = cmd & " " & arg
Next

xapp.ShellExecute xshl.ExpandEnvironmentStrings("%PAD%\DEV\vim9.0\vim90\gvim.exe"), cmd, "", "", 3
