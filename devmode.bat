@echo off

if ":%~1" == ":" goto :help
if "%~1" == "/?" goto :help
if "%~1" == "?" goto :help

if "%~1" == "exec" goto :exec

set "IFANYOF=setlocal enabledelayedexpansion&set SEL=&set IN=&call :switch" & set "SELECT=&&set "SEL=!CASE!"" & set "SETENV=endlocal&set "

rem �஢���� ⨯ ����� (�����㬥��਩/��祩�)
%IFANYOF% "cmake global gow svn graphviz doxygen ctags doc" %IN% "%~1" %SELECT%
%SETENV% TOOL=%SEL%

if ":%TOOL%" == ":" goto :toolchain
call :tool_%TOOL% %2 %3 %4 %5 %6 %7 %8 %9
exit /b %errorlevel%

:toolchain
rem ��� ��祩��� ����饭� ᬥ訢���� �। �믮������.
if not ":%DC%" == ":" echo Only one toolchain allowed!& echo Selected:%DC%& exit /b 500

if not "%~1" == "mingw" goto :n0
if "%~2" == "/?" (
    echo %~1 [*gcc/tdm/embr] [*x86/x64] [release/*debug] [unicode,static] [win7/*winxp] [cmake params]
    echo example: devmode mingw tdm x64 release unicode static win7
    exit /b 0
)
set CMAKE_BUILD_TYPE=Debug
rem set CMAKE_TOOLCHAIN_FILE=%~dp0tc\mingw.cmake

rem ��騥 ��樨 ��� ��� ���䨣��権
set "dm_v=0x0501"
set "dm_defs=-D_WINNT -D_WIN32 -DWIN32 -D_WIN32_WINNT=%%dm_v%% -D_WIN32_IE=%%dm_v%% -DWINVER=%%dm_v%%"

rem ����ன�� ���������
setlocal enabledelayedexpansion
set "a=x86"   & call :switch "x86 x64"               "%*" && set "a=!CASE!"
set "b=gcc"   & call :switch "gcc tdm embr"          "%*" && set "b=!CASE!"
set "c=Debug" & call :switch "Release Debug RDebug"  "%*" && set "c=!CASE!"
set "d=winxp" & call :switch "win7 winxp"            "%*" && set "d=!CASE!"
endlocal & set "dm_arch=%a%" & set "dm_type=%b%" & set "CMAKE_BUILD_TYPE=%c%" & set "dm_win=%d%"

rem ���⥪��୮-����ᨬ� 䫠�� ������ � cmake
if ":%dm_arch%" == ":x86" (
    set "dm_defs=-m32 -march=i686 %dm_defs%"
    set "RCFLAGS=-F pe-i386"
    set "CMAKE_SYSTEM_PROCESSOR=x86"
    set "CMAKE_SYSTEM_ARCH=x86"
)
if ":%dm_arch%" == ":x64" (
    set "dm_defs=-m64 -march=x86-64 -D_WIN64=%%dm_v%% %dm_defs%"
    set "RCFLAGS=-F pe-x86-64"
    set "CMAKE_SYSTEM_PROCESSOR=x86_64"
    set "CMAKE_SYSTEM_ARCH=x86_64"
)

rem ����� ����� ��� ���������� � 蠡���� dm_defs
if ":%dm_win%" == ":win7" set "dm_v=0x0601"

rem ����ன�� ���ᨨ ᡮન
setlocal enabledelayedexpansion
set "a="      & call :switch "static"     "%*" && set "a=!CASE!"
set "b="      & call :switch "unicode"    "%*" && set "b=!CASE!"
endlocal & set "dm_static=%a%" & set "dm_unicode=%b%"

rem ��� ����᪮� �������� �㦭� 䫠�� ������
if ":%dm_static%" == ":static" set "dm_defs=-static-libstdc++ -static-libgcc -static %dm_defs%"

rem ����� � �����প�� ����?
if ":%dm_unicode%" == ":unicode" set "dm_defs=%dm_defs% -DUNICODE -D_UNICODE"

rem ���㠫����㥬 蠡��� 䫠��� � ᮮ⢥��⢨� � ��࠭�묨 ����ன����
call set "dm_defs=%dm_defs%"

set "CFLAGS=%dm_defs%"
set "CXXFLAGS=%dm_defs%"
set "dm_build=mingw-%dm_type%-%dm_arch%-%CMAKE_BUILD_TYPE:~0,1%"

if not ":%dm_static%" == ":" set "dm_build=%dm_build%%dm_static:~0,1%"
if not ":%dm_unicode%" == ":" set "dm_build=%dm_build%%dm_unicode:~0,1%%"
if not ":%dm_win%" == ":winxp" set "dm_build=%dm_build%-w7"


if ":%dm_type%" == ":gcc" (
    set "MINGWDIR=%PAD%\DEV\MinGW"
    set "MINGWVER=%dm_build% 6.3.0 :: %CMAKE_BUILD_TYPE% %dm_static% %dm_unicode% %dm_win%"
    set "DM_INSTALL=%PAD%/DEV/MinGW/usr/local"
)

if ":%dm_type%" == ":tdm" (
    set "MINGWDIR=Q:\TDM-GCC-64"
    set "MINGWVER=%dm_build% 10.3.0 :: %CMAKE_BUILD_TYPE% %dm_static% %dm_unicode% %dm_win%"
    call :mount "Q:\TDM-GCC-64" || exit /b 2
    call :mountpkg "Q:\TDM-GCC-64-PKG"
    set "DM_INSTALL=Q:\TDM-GCC-64-PKG\"
)

if ":%dm_type%" == ":embr" (
    set "MINGWDIR=%PAD%\DEV\Embarcadero\TDM-GCC-64"
    set "MINGWVER=%dm_build% 9.2.0 :: %CMAKE_BUILD_TYPE% %dm_static% %dm_unicode% %dm_win%"
    set "DM_INSTALL=%PAD%\DEV\Embarcadero\TDM-GCC-64\PKG\"
)


if ":%MINGWDIR%" == ":" (
    echo No setup mingw dir!
    exit /b 2
)

rem �ਬ������ ����஥�
call :apply "%MINGWVER%" "%MINGWDIR%\bin" "."
set DC=%dm_build%
doskey make=mingw32-make.exe $*
set CXX=g++.exe
set CC=gcc.exe

rem ��।�� ��ࠬ���� ��� cmake (⮫쪮 �㦭�)
call :slice "Release Debug CodeBlocks Ninja" %*
call :tool_cmake %CASE%
exit /b %errorlevel%

:n0
if not "%~1" == "llvm" goto :n1
rem --------------------------------------------------------------------------
if "%~2" == "/?" echo %~1 [*win32/win64] & exit /b 0
set dm_arch=win32
if not ":%~2" == ":" set dm_arch=%~2
call :apply "LLMV 14.0.0" "Q:\LLVM\LLVM-14.0.0-%dm_arch%\bin" "."
set DC=llvm-%dm_arch%
set CXX=clang++.exe
set CC=clang.exe
exit /b %errorlevel%

:n1
if not "%~1" == "clang" goto :nx
rem --------------------------------------------------------------------------
if "%~2" == "/?" (
    echo %~1 [release/*debug] [win7/*winxp] [x86*/x64] [static] [unicode]
    echo example: devmode clang release
    exit /b 0
)

rem ��騥 ��樨 ��� ��� ���䨣��権
set "dm_v=0x0501"
set "dm_defs=-D_WINNT -D_WIN32 -DWIN32 -D_WIN32_WINNT=%%dm_v%% -D_WIN32_IE=%%dm_v%% -DWINVER=%%dm_v%%"

rem ����ன�� ���������
setlocal enabledelayedexpansion
set "a=x86"   & call :switch "x86 x64"          "%*" && set "a=!CASE!"
set "b=Debug" & call :switch "Release Debug"    "%*" && set "b=!CASE!"
set "c=winxp" & call :switch "win7 winxp"       "%*" && set "c=!CASE!"
endlocal & set "dm_arch=%a%" & set "dm_build=%b%" & set "dm_win=%c%"

set CMAKE_BUILD_TYPE=%dm_build%
if ":%dm_build%" == ":Debug" set dm_defs=%dm_defs% -g -gdwarf-3

set DC=clang
set CXX=clang++.exe
set CC=clang.exe

rem ���⥪��୮-����ᨬ� 䫠�� ������ � cmake
if ":%dm_arch%" == ":x86" (
    set "dm_defs=-m32 -march=i686 %dm_defs%"
    set "RCFLAGS=-F pe-i386"
    set DC=clang-x86
    set CXX=i686-w64-mingw32-clang++.exe
    set CC=i686-w64-mingw32-clang.exe
    set "CMAKE_SYSTEM_PROCESSOR=x86"
    set "CMAKE_SYSTEM_ARCH=x86"
)
if ":%dm_arch%" == ":x64" (
    set "dm_defs=-m64 -march=x86-64 %dm_defs%"
    set "RCFLAGS=-F pe-x86-64"
    set DC=clang-x64
    set CXX=x86_64-w64-mingw32-clang++.exe
    set CC=x86_64-w64-mingw32-clang.exe
    set "CMAKE_SYSTEM_PROCESSOR=x86_64"
    set "CMAKE_SYSTEM_ARCH=x86_64"
)

rem ����� ����� ��� ���������� � 蠡���� dm_defs
if ":%dm_win%" == ":win7" set "dm_v=0x0601"

rem ����ன�� ���ᨨ ᡮન
setlocal enabledelayedexpansion
set "a="      & call :switch "static"     "%*" && set "a=!CASE!"
set "b="      & call :switch "unicode"    "%*" && set "b=!CASE!"
endlocal & set "dm_static=%a%" & set "dm_unicode=%b%"

rem ��� ����᪮� �������� �㦭� 䫠�� ������
if ":%dm_static%" == ":static" (
    set CMAKE_EXE_LINKER_FLAGS=-static
    set LDFLAGS=-static
)

rem ����� � �����প�� ����?
if ":%dm_unicode%" == ":unicode" set "dm_defs=%dm_defs% -DUNICODE -D_UNICODE"

rem ���㠫����㥬 蠡��� 䫠��� � ᮮ⢥��⢨� � ��࠭�묨 ����ன����
call set "dm_defs=%dm_defs%"

set "CFLAGS=%dm_defs%"
set "CXXFLAGS=%dm_defs%"
set "DC=clang-%dm_arch%-%dm_build:~0,1%"

if not ":%dm_static%" == ":" set "DC=%DC%-S"
if not ":%dm_unicode%" == ":" set "DC=%DC%-U"
if ":%dm_win%" == ":winxp" set "DC=%DC%-XP"

call :apply "CLANG U++" "%PAD%\DEV\clang\bin" "."


rem ��।�� ��ࠬ���� ��� cmake (⮫쪮 �㦭�)
call :slice "Release Debug CodeBlocks Ninja" %*
call :tool_cmake %CASE%
exit /b %errorlevel%

:nx
if not "%~1" == "gow" goto :n2
rem --------------------------------------------------------------------------
:tool_gow
if "%~2" == "/?" echo %~1 has no opts. & exit /b 0
call :apply "GOW (Gnu on Windows)" "%PAD%\SYS\GOW" "."
exit /b %errorlevel%

:n2
if not "%~1" == "psdk" goto :n3
rem --------------------------------------------------------------------------
if "%~2" == "/?" echo %~1 [2000/*XP32/XP64/SRV32/SRV64/X64] [*RETAIL/DEBUG] & exit /b 0

setlocal enabledelayedexpansion
set "a=XP32"   & call :switch "2000 XP64 SRV32 SRV64 x64" %* && set "a=!CASE!"
set "b=RETAIL" & call :switch "DEBUG RELEASE"             %* && set "b=!CASE!"
set "c=x86"    & call :switch "XP64 SRV64 x64"            %* && set "c=x64"
if ":%a%" == ":x64" set "a=XP64"
if ":%b%" == ":RELEASE" set "b=RETAIL"
echo select %~1: %a% %b%
endlocal & set "dm_psdk=%a%" & set "dm_type=%b%" & set "dm_arch=%c%"

rem ��騥 䫠�� psdk
set CFLAGS=-DCRTAPI1=_cdecl -DCRTAPI2=_cdecl -nologo -GS -DWIN32 -D_WIN32 -D_WIN32_IE=0x0600
rem ���⥪��୮-����ᨬ� 䫠�� ������ � cmake
if ":%dm_arch%" == ":x86" (
    set CFLAGS=-D_X86_=1 -W3 -D_WINNT -D_WIN32_WINNT=0x0501 -DWINVER=0x0501
    set "CMAKE_SYSTEM_PROCESSOR=x86"
    set "CMAKE_SYSTEM_ARCH=x86"
    call :mount Q:\VC2003 && call Q:\VC2003\vcvars.bat
)
if ":%dm_arch%" == ":x64" (
    set CFLAGS=%CFLAGS% -D_AMD64_=1 -DWIN64 -D_WIN64 /FIPRE64PRA.H -Wp64 -W4 -D_WINNT -D_WIN32_WINNT=0x0502 -DWINVER=0x0502 -D_MT -MT
    set "CMAKE_SYSTEM_PROCESSOR=AMD64"
    set "CMAKE_SYSTEM_ARCH=AMD64"
    set "CMAKE_TOOLCHAIN_FILE=%~dp0tc/psdk-64.cmake"
)
set CXXFLAGS=%CFLAGS%

call :mount Q:\PSDK && call Q:\PSDK\SetEnv.Cmd /%dm_psdk% /%dm_type%

rem DirectX DSK
set LIB=%LIB%;Q:\DEVPACKS\VC2003\lib
set INCLUDE=%INCLUDE%;Q:\DEVPACKS\VC2003\include

set CMAKE_GENERATOR=NMake Makefiles
set DC=PSDK-%dm_psdk%-%dm_type%
set DC=%DC: =%
doskey make=nmake.exe /NOLOGO OUTDIR=BUILD\%DC% $*
set CXX=cl.exe
set CC=cl.exe
exit /b %errorlevel%
:n3

if not "%~1" == "msys" goto :n4
rem --------------------------------------------------------------------------
if defined DISPLAY set DISPLAY=
set CMAKE_GENERATOR=MSYS Makefiles
set WD=c:\MinGW\msys\1.0\bin\
set DC=msys
set CC=gcc
set CXX=g++
%WD%sh --login
exit /b %errorlevel%
:n4

if not "%~1" == "cmake" goto :n5
:tool_cmake
echo.
echo -------- setup tool cmake ---------------------------------------------------
echo -    params: %*
echo -----------------------------------------------------------------------------
rem ������ 䠩� � ��������� ᡮન (��� LSP)
set CMAKE_EXPORT_COMPILE_COMMANDS=1
set CMAKE_SYSTEM_NAME=Windows
call :apply "CMAKE 3.22.3" "%~dp0devmode;%PAD%\DEV\cmake-3.22.3-windows-i386\bin" "."
set DM_OUT=BUILD\%DC%
set DM_DEF=-Wdev
if ":%DM_INSTALL%" == ":" goto :no_dm_install
if not exist "%DM_INSTALL%\" echo Warning: DM_INSTALL incorrect (%DM_INSTALL%)
set DM_DEF=%DM_DEF% -DCMAKE_INSTALL_PREFIX="%DM_INSTALL%"
:no_dm_install
rem �� ���塞 �������, �᫨ �� �� 㪠��� ��祩���
if not ":%CMAKE_GENERATOR%" == ":" goto :skip_gen
rem ����ன�� ������� cmake
setlocal enabledelayedexpansion
set "a=Mingw"      & call :switch "Ninja CodeBlocks" "%*" && set "a=!CASE!"
endlocal & set "dm_generator=%a%"
rem ��ࠢ�塞 ⥣ �� ॠ��� �������
if ":%dm_generator%" == ":CodeBlocks" set "dm_generator=CodeBlocks - MinGW Makefiles"
if ":%dm_generator%" == ":Mingw" set "dm_generator=MinGW Makefiles"
if not ":%dm_generator%" == ":" set "CMAKE_GENERATOR=%dm_generator%"
:skip_gen
echo - variables:
echo -     DM_OUT = %DM_OUT%
echo -     DM_DEF = %DM_DEF%
echo -     DM_INSTALL = %DM_INSTALL%
echo -     CMAKE_BUILD_TYPE = %CMAKE_BUILD_TYPE%
echo -     CMAKE_TOOLCHAIN_FILE = %CMAKE_TOOLCHAIN_FILE%
echo -     CMAKE_SYSTEM_PROCESSOR = %CMAKE_SYSTEM_PROCESSOR%
echo -     CMAKE_SYSTEM_ARCH = %CMAKE_SYSTEM_ARCH%
echo -     CMAKE_GENERATOR = %CMAKE_GENERATOR%
echo -  examples:
echo -     cmake -B %%DM_OUT%% -S . %%DM_DEF%%
echo -     cmake --build %%DM_OUT%%
echo -     cmake --install %%DM_OUT%%
echo -     hints:
echo -       * use configure/build/install to call cmake with selected config
echo -------- end of cmake -------------------------------------------------------
exit /b 0
:n5

if not "%~1" == "global" goto :n6
rem --------------------------------------------------------------------------
:tool_global
call :apply "GLOBAL 6.6.8" "%PAD%\SYS\global\bin\" "."
exit /b 0
:n6

if not "%~1" == "svn" goto :n7
rem --------------------------------------------------------------------------
:tool_svn
call :apply "SVN" "%PAD%\DEV\SSVN\bin\" "."
exit /b 0
:n7


if not "%~1" == "masm32" goto :n8
if "%~2" == "/?" (
    echo usage: devmode %~1 [release/*debug] [Console/Win32]
    exit /b 0
)
call :mount Q:\masm32
if not exist "Q:\masm32\" exit /b -1
call :apply "MASM32" "Q:\masm32\bin\" "."

rem ����ன�� ���������
setlocal enabledelayedexpansion
set "a=x86"     & call :switch "x86 x64"          "%*" && set "a=!CASE!"
set "b=Debug"   & call :switch "Release Debug"    "%*" && set "b=!CASE!"
set "c=Console" & call :switch "Console Win32"    "%*" && set "c=!CASE!"
endlocal & set "dm_arch=%a%" & set "dm_type=%b%" & set "dm_target=%c%"

set LINK_FLAGS=/SUBSYSTEM:CONSOLE
set ASM_FLAGS=/coff
set INCLUDE=Q:\masm32\include

if ":%dm_target%" == ":Win32" set LINK_FLAGS=/SUBSYSTEM:WINDOWS

if ":%dm_type%" == ":Debug" (
    set LINK_FLAGS=%LINK_FLAGS% /DEBUG /DEBUGTYPE:CV
    set ASM_FLAGS=%ASM_FLAGS% /Zd /Zf /Zi /nologo
)
if ":%dm_type%" == ":Release" (
    set LINK_FLAGS=%LINK_FLAGS% /RELEASE
    set ASM_FLAGS=%ASM_FLAGS% /nologo
)

set LINK_FLAGS=%LINK_FLAGS% /LIBPATH:Q:\masm32\lib /NOLOGO
set DM_OUT=BUILD\masm32
exit /b 0
:n8


if not "%~1" == "graphviz" goto :n9
rem --------------------------------------------------------------------------
:tool_graphviz
call :apply "GRAPHVIZ 2.38" "%PAD%\DEV\graphviz-2.38\bin" "."
exit /b 0
:n9


if not "%~1" == "doxygen" goto :n10
rem --------------------------------------------------------------------------
:tool_doxygen
call :apply "DOXYGEN 1.8.7" "%PAD%\DEV\doxygen\bin" "."
exit /b 0
:n10

if not "%~1" == "ctags" goto :n11
rem --------------------------------------------------------------------------
:tool_ctags
call :apply "CTAGS 5.8" "%PAD%\DEV\ctags\" "."
exit /b 0
:n11


if not "%~1" == "doc" goto :n12
rem --------------------------------------------------------------------------
:tool_doc
echo.
echo -----------  ENABLE DOC TOOLS: GLOBAL CTAGS GRAPHVIZ DOXYGEN  ---------------
for %%i in (global ctags graphviz doxygen) do (
    call :tool_%%i
)
exit /b 0
:n12




echo Error: unsupported toolchain [%~1]. & exit /b 1
goto :eof

rem ��ࠬ����: "tag1 tag2...tagN" "��ப� ����� � ��������� ����஬ ⥣��"
rem �뤥��� �� ��ப� ����� ���� �������� ⥣ ��� ���� ॣ���� � ����頥�
rem ��� ��ଠ���� ��� � ��६����� CASE, �����頥� �㫥��� ��� �ᯥ�.
:switch
    setlocal enabledelayedexpansion
    if ":%~2%~3%~4%~5%~6%~7%~8%~9" == ":" exit /b 1
    set "CA=%~1 "
    set "CA=%CA:     = %"
    set "CA=%CA:    = %"
    set "CA=%CA:   = %"
    set "CA=%CA:  = %"
    set "CA=%CA:~0,-1%"
    set "CA=echo %CA: =&echo %"
    set "ESCP="
    for %%i in (%~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9) do set "ESCP=!ESCP! \^<%%i\^>"
    set "ESCP=%ESCP:~1%"
    set "CASE="
    for /F "tokens=* usebackq" %%i in (`@cmd /c "%CA%" ^| findstr /I /R "%ESCP%"`) do (
        if ":!CASE!" == ":" set "CASE=%%i"
    )
    if ":!CASE!" == ":" exit /b 2
    endlocal & set "CASE=%CASE: =%"
    exit /b 0

:slice
    setlocal enabledelayedexpansion
    if ":%~2%~3%~4%~5%~6%~7%~8%~9" == ":" exit /b 1
    set "CA=%~1 "
    set "CA=%CA:     = %"
    set "CA=%CA:    = %"
    set "CA=%CA:   = %"
    set "CA=%CA:  = %"
    set "CA=%CA:~0,-1%"
    set "CA=echo %CA: =&echo %"
    set "ESCP="
    for %%i in (%~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9) do set "ESCP=!ESCP! \^<%%i\^>"
    set "ESCP=%ESCP:~1%"
    set "CASE="
    for /F "tokens=* usebackq" %%i in (`@cmd /c "%CA%" ^| findstr /I /R "%ESCP%"`) do (
        set "CASE=!CASE! %%i"
    )
    if ":!CASE!" == ":" exit /b 2
    endlocal & set "CASE=%CASE%"
    exit /b 0

:set_if
    call :is_varname "%~1" || (
        echo error: set_if accept only [A-Z] as var name!
        exit /b -1
    )
    setlocal
    set "var=%~1"
    shift
    set "sel=%~1"
    shift
    :set_if_loop
    if "%~1"=="" goto :set_if_end
    for /f "tokens=1,* delims=:" %%A in ("%~1") do (
        if /i "%sel%"=="%%A" endlocal & set "%var%=%%B" & exit /b 0
    )
    shift
    goto :set_if_loop
    :set_if_end
    endlocal & exit /b -1

:is_varname
    setlocal enabledelayedexpansion
    set var=%~1
    for /l %%i in (0,1,255) do (
        set "char=!var:~%%i,1!"
        if "!char!" EQU "" endlocal & exit /b 0
        if "!char!" NEQ "_" (
            if "!char!" LSS "A" endlocal & exit /b -1
            if "!char!" GTR "Z" endlocal & exit /b -1
        )
    )
    endlocal & exit /b -1

:strpart
    set /a RETURN=%~1+1
    call set RETURN=%%~%RETURN%
    exit /b 0

:mountpkg
    if exist "%~1/" exit /b 0
    pfm mount -c -w "%~1"
    exit /b %errorlevel%

:mount
    if exist "%~1/" exit /b 0
    pfm mount -r -c -f PFMZIPFS "%~1"
    exit /b %errorlevel%

:apply
    if defined DC echo - override devmode "%DC%" with "%~1"
    echo -   version: %~1
    set WD=%~f3
    echo - add paths: %~2
    if not ":%DM_PATH%" == ":" set "DM_PATH=%~2;%DM_PATH%"
    if ":%DM_PATH%" == ":" set "DM_PATH=%~2"
    cd /d "%WD%" || exit /b 1
    set "PATH=%~2;%PATH%"
    exit /b 0

:help
    echo usage: devmode [toolchain/tool] [args or /?]
    echo example: devmode psdk /?
    echo toolchains: llvm clang psdk msys mingw masm32
    echo tools: gow cmake global graphviz svn doxygen
    echo.
    if defined DM_TOOL echo devmode tool: "%DM_TOOL%"
    if defined DM_PATH echo devmode path: "%DM_PATH%"
    exit /b 0

:exec
    @dir /B /ON /S %DM_OUT%\*.exe
    @CMD /K set "PATH=%%DM_OUT%%;%%PATH%%"
    exit /b 0

:eof
