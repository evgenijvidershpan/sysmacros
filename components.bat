@echo off
if ":%~1" == ":" goto :help
if "%~1" == "/?" goto :help

setlocal enabledelayedexpansion
set "da=--dissable-ts"
set "db=--with-long-opt"
set "dc=--semantic"
set ext=
call :switch  "ts" "%*" && set ext=!ext! ^& set "A=%da%"
call :switch "opt" "%*" && set ext=!ext! ^& set "B=%db%"
call :switch "smt" "%*" && set ext=!ext! ^& set "C=%dc%"
endlocal %ext%
echo select: %A% %B% %C%
exit /b 0

:switch
    setlocal enabledelayedexpansion
    if ":%~2%~3%~4%~5%~6%~7%~8%~9" == ":" exit /b 1
    set CA=%~1
    set CA=echo %CA: =^&echo %
    set "CASE="
    for /F "tokens=* usebackq" %%i in (`cmd /c "(%CA%) | findstr /I /L "%~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9""`) do @set "CASE=!CASE! %%i"
    if ":!CASE!" == ":" exit /b 2
    set CASE=%CASE:  = %
    set CASE=%CASE:~1,-1%
    endlocal & set "CASE=%CASE%"
    exit /b 0

:help
    echo usage: components [ts opt smt]
    echo.
    if not ":%A%" == ":" echo  TS: %A%
    if not ":%B%" == ":" echo OPT: %B%
    if not ":%C%" == ":" echo SMT: %C%
    exit /b 0

