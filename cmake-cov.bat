@echo off
setlocal enabledelayedexpansion

if ":%DM_OUT%" == ":" (
    echo ERR: DM_OUT is not set!
    echo exiting...
    exit /b -1
)

if not exist "%DM_OUT%/CMakeCache.txt" (
    echo ERR: cmake cache not found!
    echo exiting...
    exit /b -1
)

set "WD=%CD%"
set "FUNC=:decode"

if ":%~1" == ":reset" set "FUNC=:reset"

for /R "%DM_OUT%/" %%i in (*.gcno) do (
    call %FUNC% "%%i"
)

cd /d "%WD%"
endlocal
exit /b 0

:decode
    set "CF=%~dpn1.gcov"
    echo create: %CF:\=/%
    cd /d "%~dp1" || (
        echo ERR: fail cd to: "%~dp1"
        exit /b -1
    )
    gcov "%~n1.obj"
    cd /d "%WD%"
    exit /b 0

:reset
    echo delete: %~dpn1.gcov
    del /F /Q "%~dpn1.gcov" || (
        echo ERR: fail delete: "%~dp1"
        exit /b -1
    )
    echo delete: %~dpn1.gcda
    del /F /Q "%~dpn1.gcda" || (
        echo ERR: fail delete: "%~dp1"
        exit /b -1
    )
    exit /b 0


