@echo off
set CXXFLAGS=NONE
call :set_if CXXFLAGS "%~1"^
    "Debug:-g"^
    "Release:-O3" ^
    "RDebug:-O3 -g -gline-tables-only" ^
    || echo no args!

echo %CXXFLAGS%
exit /b 0

:set_if
    call :is_varname "%~1" || (
        echo error: set_if accept only [A-Z] as var name!
        exit /b -1
    )
    setlocal
    set "var=%~1"
    shift
    set "sel=%~1"
    shift
    :set_if_loop
    if "%~1"=="" goto :set_if_end
    for /f "tokens=1,* delims=:" %%A in ("%~1") do (
        if /i "%sel%"=="%%A" endlocal & set "%var%=%%B" & exit /b 0
    )
    shift
    goto :set_if_loop
    :set_if_end
    endlocal & exit /b -1

:is_varname
    setlocal enabledelayedexpansion
    set var=%~1
    for /l %%i in (0,1,255) do (
        set "char=!var:~%%i,1!"
        if "!char!" EQU "" exit /b 0
        if "!char!" NEQ "_" (
            if "!char!" LSS "A" exit /b -1
            if "!char!" GTR "Z" exit /b -1
        )
    )
    endlocal
    exit /b 0


