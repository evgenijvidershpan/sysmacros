@echo off

if ":%~1" == ":" goto :help
if ":%~2" == ":" goto :help

set "SRC=%~dpf1"
set "TG=%~dpf2"

echo ------------------------ BEGIN MIRRORING -------------------------------------
echo - FROM: %SRC%
echo -   TO: %TG%
echo ------------------------------------------------------------------------------
if not exist "%SRC%\" echo "incorrect source dir: %SRC%" & exit /b -1
if not exist "%TG%\" echo "incorrect destination dir: %TG%" & exit /b -1

set WD=%CD%
git --version || (
    echo "git not available!"
    exit /b -1
)
cd /d "%SRC%" || (
    echo "source dir not readdable: %SRC%"
    exit /b -2
)

for /d %%i in (*) do  call :gitmirror "%%i" %3 %4 %5 %6 %7 %8 %9

cd /d "%WD%"
echo ----------------------------- END --------------------------------------------
pause
exit /b 0

:help
    echo usage: %~n0 source-dir target-dir [git args]
    exit /b -1

:gitmirror
    echo.
    echo ------- MIRROR: %~f1
    if not exist ".\%~1\config" exit /b -99
    cd "%~1" || exit /b -33
    if not exist "%TG%\%~1\" (
        git clone --mirror -- . "%TG%\%~1" %2 %3 %4 %5 %6 %7 %8 %9
    )
    git push --mirror --repo="%TG%\%~1" %2 %3 %4 %5 %6 %7 %8 %9
    cd ..
    echo ------- DONE
    exit /b 0

rem Производит передачу обновлений всех локальных репозитариев во внешнее
rem хранилище на накопителе (текущая директория запуска) что намного быстрее
rem копирования их всех вручную...

