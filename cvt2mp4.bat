@echo off
setlocal enabledelayedexpansion

SET LOGFILE=convert-log.txt
SET PATH=%PATH%;%PAD%\media\ffmpeg\bin

SET A=-codec:a aac -strict -2 -b:a 384k -r:a 48000
SET V=-codec:v libx264 -crf 21 -bf 2 -flags +cgop -pix_fmt yuv420p
SET YF=-movflags faststart
SET PFX=

IF "x%1" EQU "x" (
    echo usage: %~n0 [*.avi^|*.jdp^|*.mpg^|*.etc]
    EXIT /b 1
)

CALL :Preset %*

SET emsg=ffmpeg params: %PFX% -i {INPUT} %V% %A% %YF% {OUTPUT}
ECHO %emsg%

call :logwrite "------------- start new converting -----------------"
call :logwrite "%emsg%"

rem �஢�ઠ, �� ��� ��।��� ������� 䠩� � �� ����.
if "%~nx1" EQU "%~1" (
    call :Convert "%~1"
    goto shutdown_alarm
)

for %%i in (%~1) do call :Convert "%%i"

:shutdown_alarm
rem set 15 min (900 sec) to shutdown!
call :ShutdownAlarm 900
exit /b 0

echo --------------------------       EOF       ------------------------------
goto Infinity

:Preset
    SET _EXT=%~x1
    CALL :TOUPPERCASE _EXT
    IF "%_EXT%" EQU ".JDP" (
        SET A=
        SET PFX=-r 30 -f image2pipe -vcodec mjpeg
    )
    exit /b 0

:ShutdownAlarm
    SET SA=%1
    for /L %%i in (0,1,%1) do (
        <nul SET /p=time to shutdown: !SA! 
        1> nul 2>&1 ping 127.0.0.1 /n 2
        SET /a "SA=!SA!-1"
    )
    shutdown -f -s -t 60
    exit /b 0


:Convert
    if exist "%~1.mp4" (
        rem call :LogWrite "scip converted: %1"
        exit /b 1
    )
    if exist "_%~1.mp4" (
        rem call :LogWrite "scip in progress: %1"
        exit /b 2
    )
    call :LogWrite "start convert: %~1"
    ffmpeg %PFX% -i "%~1" %V% %A% %YF% "_%~1.mp4" && ren "_%~1.mp4" "%~1.mp4"
    if not exist %1.mp4 (
        call :LogWrite "error convert: %~1"
        exit /b 3
    )
    call :LogWrite "end convert: %~1"
    exit /b 0


:LogWrite
    if "x!LOGFILE!" EQU "x" exit /b 1
    echo %DATE% %TIME%	%~1 >> !LOGFILE!
    exit /b 0

:TOUPPERCASE
    if not defined %~1 exit /b 1
    for %%a in ("a=A" "b=B" "c=C" "d=D" "e=E" "f=F" "g=G" "h=H" "i=I" "j=J" "k=K" "l=L" "m=M" "n=N" "o=O" "p=P" "q=Q" "r=R" "s=S" "t=T" "u=U" "v=V" "w=W" "x=X" "y=Y" "z=Z" ) do call set %~1=%%%~1:%%~a%%
    exit /b 0


:Infinity
    echo hang on %emsg%
    pause
    goto Infinity


