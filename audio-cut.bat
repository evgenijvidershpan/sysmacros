@echo off
set path=%path%;%PAD%\media\ffmpeg\bin
:ask
SET /p "input=Input file name :> " || goto :ask
SET /p "output=Output file name :> " || set output=~%input%
SET /p "timings=Timings: (example cut 5 sec from 37sec: -ss 00:37:000 -t[o] 00:05:000) :> "

ffmpeg %timings% -i %input% -vn -acodec copy %output%
pause

