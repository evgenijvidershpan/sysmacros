@echo off
if ":%~1" == ":" goto :help
if "%~1" == "/?" goto :help

if ":%PAD%" == ":" (
    echo ERR: PAD ^(Path to App Dir^) is not set!
    exit /b -1
)

:repeat
setlocal enabledelayedexpansion
call switch-string "imagemagic perl ffmpeg" "%~1" || (
    echo ERR: not supported tool: %~1
    goto :shift
)
endlocal & call :tool_%CASE%
:shift
shift
if not ":%~1" == ":" goto :repeat
exit /b 0


:tool_imagemagic
    set "IMG=%PAD%\MEDIA\ImageMagick-7.1.1-19.zip"
    call :mount "%IMG%" && (
        set "PATH=%IMG%;%PATH%"
        exit /b 0
    )
    exit /b -1


:tool_perl
    set "IMG=%PAD%\DEV\strawberry-perl-5.32.1.1-32bit-portable.zip"
    call :mount "%IMG%" && (
        set "PATHEXT=%PATHEXT%;.PERL"
        call "%IMG%\portableshell.bat" /SETENV
        exit /b 0
    )
    exit /b -1


:tool_ffmpeg
    set PATH=%PATH%;%PAD%\MEDIA\FFMPEG\bin
    exit /b 0



:mount
    if exist "%~1/" (
        echo image mounted: %~1
        exit /b 0
    )
    echo mount image: %~1
    pfm mount -r -c -f PFMZIPFS "%~1" || (
        echo ERR: fail mount img!
        exit /b -1
    )
    exit /b 0


:help
    echo.
    echo usage: require-tool [tool-a] [tool-b] ...
    echo tools: imagemagic perl ffmpeg
    exit /b 0
