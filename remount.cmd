@echo off
setlocal

rem base path = script path
set basepath=%~dp0
rem without last '\'
set basepath=%basepath:~0,-1%

set version=%~n0
set mountpath=%basepath%\%version%
set baseimg=%basepath%\%version%.base

set baseimg=%baseimg:&=$%

set subdisk=%basepath%\%version%.subdisk
set script=%temp%\%~n0.dpscript

call :EnsureDirExist "%basepath%"
call :EnsureDirExist "%mountpath%"

for /F %%i in ('dir /b "%mountpath%\*"') do (
  goto dismount_subdisk
)

call :EnsureDirEmpty "%mountpath%"

if exist "%subdisk%" goto mount_subdisk
if exist "%baseimg%" goto create_subdisk


:create_maindisk
echo ----------------- create maindisk ----------------------

set sizeof=256
:ask
set /p "sizeof=����쪮 ��������: " || goto :ask
set /a "x10=%sizeof%*10"
if "%x10%" EQU "%sizeof%0" goto :next
ECHO ������� �� �᫮, ���஡�� ��� ࠧ.
goto :ask
:next

call :EnsureRemoveFile "%script%"
echo create script...
if errorlevel 0 (
  echo create vdisk file="%baseimg%" maximum=%sizeof% type=expandable
  echo select vdisk file="%baseimg%"
  echo attach vdisk
  echo convert mbr
  echo create partition primary
  echo attributes volume set nodefaultdriveletter
  echo format fs=ntfs label="%~n0" quick
  echo assign mount="%mountpath%"
) > "%script%"

echo exec create maindisk script.
diskpart /s "%script%"

call :EnsureDirExist "%mountpath%"

1> nul 2>&1 compact /C /S /A /I /F /Q "%mountpath%\"

echo now install app and press any key.

:install
pause
for /F %%i in ('dir /b "%mountpath%\*"') do goto unmount
echo dir: "%mountpath%" is empty!
goto install
:unmount

call :EnsureRemoveFile "%script%"

if exist "%mountpath%\" (
  echo select vdisk file="%baseimg%"
  echo select volume="%mountpath%"
  echo remove all dismount noerr
  echo detach vdisk
) > "%script%"

echo exec maindisk detach script...
diskpart /s "%script%"

call :EnsureDirEmpty "%mountpath%"
exit /b 0


:create_subdisk
echo ----------------- create subdisk ------------------------
call :EnsureExistFile "%baseimg%"

call :EnsureRemoveFile "%script%"
echo create script...
if errorlevel 0 (
  echo create vdisk file="%subdisk%" parent="%baseimg%"
) > "%script%"

echo exec create subdisk script...
diskpart /s "%script%"

if exist "%subdisk%" goto mount_subdisk

set emsg=create subdisk fail!
goto infinity


:mount_subdisk
echo ---------------- mount subdisk ---------------------------
call :EnsureExistFile "%baseimg%"
call :EnsureExistFile "%subdisk%"

call :EnsureRemoveFile "%script%"
if not exist "%script%" (
  echo select vdisk file="%subdisk%"
  echo attach vdisk
) > "%script%"

echo exec attach subdisk script...
diskpart /s "%script%"

call :EnsureRemoveFile "%script%"
if not exist "%script%" (
  echo select vdisk file="%subdisk%"
  echo select partition=1
  echo assign mount="%mountpath%\"
) > "%script%"

echo exec mount subdisk script...
diskpart /s "%script%"
exit /b 0


:dismount_subdisk
echo ------------ dismount subdisk -------------------------
call :EnsureExistFile "%baseimg%"
call :EnsureExistFile "%subdisk%"

call :EnsureRemoveFile "%script%"
if not exist "%script%" (
  echo select vdisk file="%subdisk%"
  echo select partition=1
  echo remove all dismount noerr
  echo detach vdisk
) > "%script%"

echo exec dismount subdisk script...
diskpart /s "%script%"
exit /b 0


echo ------------------------------ end batch --------------------------------
exit /b 0


:EnsureRemoveFile
if exist "%1" 1> nul 2>&1 del /f /q "%1"
if not exist "%1" exit /b 0
set emsg=remove: %1
goto infinity


:EnsureDirExist
if exist "%1\" exit /b 0
mkdir "%1"
if exist "%1\" exit /b 0
set emsg=dir %1 not exist
goto infinity


:EnsureDirEmpty
if not exist "%1\" (
  set emsg=dir [%1] not exist!
  goto infinity
)
for /F %%i in ('dir /b "%1\*"') do (
  set emsg=non empty dir: "%1"
  goto infinity
)
exit /b 0
goto infinity


:EnsureExistFile
if exist "%1" exit /b 0
echo Error: file not exist: %~n1%~x1
goto infinity


:infinity
echo hang on %emsg%
pause
goto infinity
