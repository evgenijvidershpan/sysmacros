@ECHO OFF

git.exe --version || goto eof

if "x%~1" == "x" exit /b 1

goto :start

:gitcmd
    echo --- FOR: %~f1
    cd "%~1/" || exit /b -1
    git %2 %3 %4 %5 %6 %7 %8 %9
    cd ..
    exit /b 0

:start

for /d %%i in (*) do call :gitcmd "%%i" %1 %2 %3 %4 %5 %6 %7 %8 %9

