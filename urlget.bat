@echo off
MKDIR %DOWNLOADS%\URLGET
SET DD=%DATE:~0,2%
SET DM=%DATE:~3,2%
SET DY=%DATE:~8,2%

REM === преобразовываем %TIME% из " 8:20:06,87" в "08:20:06,87"
SET FTIME=%TIME: =0%
REM === преобразовываем из "08:20:06,87" в "08:20"
SET FTIME=%FTIME:~0,5%
REM === преобразовываем из "08:20" в "08.20"
SET FTIME=%FTIME::=.%

SET /p FNAME=enter file name:

IF "x%FNAME%" == "x" SET FNAME=%DY%.%DM%.%DD%-%FTIME%

SET FNAME=%DOWNLOADS%\URLGET\%FNAME%
SET PATH=%PATH%;%PAD%\SYS\GOW
SET AGENT=AGENT007

wget -c --no-check-certificate --user-agent="%AGENT%" --limit-rate=4096 --input-file=url.txt -O "%FNAME%.mp4"
start OSD n download complete, saved to %FNAME%.mp4

