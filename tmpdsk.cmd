echo off
(
  echo create vdisk file="k:\tmp.vhd" maximum=1024 type=expandable
  echo select vdisk file="k:\tmp.vhd"
  echo attach vdisk
  echo convert mbr
  echo create partition primary
  echo assign letter=t
  echo format fs=ntfs label="tmp_disk" quick
) | diskpart
pause
