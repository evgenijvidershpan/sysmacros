@ECHO OFF

git.exe --version || goto eof

if "x%~1" == "x" call :SaveWD "." && goto :fetch

call :SaveWD "%~1" || goto eof
cd /d "%WD%\" || goto eof

:fetch

SET list=doss,icons,source,vim-bundle,sysmacros,trayit,fastgui,sqlite,cmuta
SET list=%list%,fpl,vimtweak,stringfx,sqlitecpp,translate,tagobj
SET list=%list%,evgenijvidershpan.bitbucket.io,h3015,cpt

for %%i in (%list%) do (
    call :RepositoryFetch "%%i"
)

goto eof

:RepositoryFetch
    pushd "%WD%"
    if not exist "%~1/" (
        echo GET: "%~1"
        git clone "git@bitbucket.org:evgenijvidershpan/%~1.git"
        popd
        exit /b 0
    )
    echo UP: "%~1"
    cd "%WD%/%~1/" && git fetch --all
    popd
    exit /b 0

:SaveWD
    set WD=%~f1
    if not exist "%WD%/" (
        echo ERR: path not exist: %WD%\
        exit /b 1
    )
    echo CD: %WD%
    exit /b 0

:eof

