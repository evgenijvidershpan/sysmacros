#!/bin/bash

if [ -z "$1" ]; then
    echo "usage: $(basename $0) source-dir target-dir [git args]"
    exit 1
fi

SRC="$1"
TG="$2"

echo "------------------------ BEGIN MIRRORING -------------------------------------"
echo "- FROM: $SRC"
echo "-   TO: $TG"
echo "------------------------------------------------------------------------------"

if [ ! -d "$SRC" ]; then
    echo "incorrect source dir: $SRC"
    exit 1
fi

if [ ! -d "$TG" ]; then
    echo "incorrect destination dir: $TG"
    exit 1
fi

WD=$(pwd)

if ! git --version &> /dev/null; then
    echo "git not available!"
    exit 1
fi

cd "$SRC" || { echo "source dir not readable: $SRC"; exit 1; }

for dir in */; do
    gitmirror "$dir" "${@:3}"
done

cd "$WD"
echo "----------------------------- END --------------------------------------------"
read -p "Press any key to continue... " -n1 -s
echo

exit 0

gitmirror() {
    echo
    echo "------- MIRROR: $1"
    if [ ! -d "$1/objects" ]; then
        echo "No git repository found in $1"
        return 1
    fi
    cd "$1" || return 1
    if [ ! -d "$TG/$1" ]; then
        git clone --mirror -- . "$TG/$1" "$@"
    else
        git push --mirror --repo="$TG/$1" "$@"
    fi
    cd ..
    echo "------- DONE"
    return 0
}


