@ECHO OFF
FOR /F "usebackq delims=" %%i IN (`git describe --long --tags --always --dirty`) DO SET VERSION=%%i
SET VERSION=#define BUILD_VERSION _T("%VERSION%")
SET /p CURRENT=< .version
if "%VERSION% " NEQ "%CURRENT%" <nul SET /p=%VERSION% > .version
EXIT 0
