@ECHO OFF

git.exe --version || goto eof

if "x%~1" == "x" call :SaveWD "." && goto :fetch

call :SaveWD "%~1" || goto eof
cd /d "%WD%\" || goto eof

:fetch

for /d %%i in (*) do call :RepositoryFetch "%%i"

goto eof

:RepositoryFetch
    if not exist "%~1/" exit /b 1
    echo ---------------------------------------------------------------------
    echo FETCH: "%~1"
    cd "%WD%/%~1/" && git fetch --all
    cd /d "%WD%"
    exit /b 0

:SaveWD
    set WD=%~f1
    if not exist "%WD%/" (
        echo ERR: path not exist: %WD%\
        exit /b 1
    )
    echo CD: %WD%
    exit /b 0

:eof

