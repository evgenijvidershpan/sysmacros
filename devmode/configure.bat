@echo off

if ":%DM_OUT%" == ":" (
    echo DM_OUT is not set! exiting...
    exit /b -1
)

if not exist "%DM_OUT%\" mkdir "%DM_OUT%"

cmake -B "%DM_OUT%" -S . %DM_DEF% %*

