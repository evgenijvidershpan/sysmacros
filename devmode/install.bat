@echo off

if not exist "%DM_OUT%\CMakeFiles\CMakeOutput.log" (
    echo no build found! exiting...
    exit /b -1
)

cmake --install "%DM_OUT%" %*

