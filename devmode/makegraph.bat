@echo off

if ":%DM_OUT%" == ":" (
    echo DM_OUT is not set!
    echo exiting...
    exit /b -1
)

if not exist "%DM_OUT%\" (
    echo DM_OUT: %DM_OUT%\ is not exist!
    echo exiting...
    exit /b -1
)

set "DM_GRAPH=%DM_OUT%\deps.dot"
set "DM_GPDF=%DM_OUT%\deps.pdf"

cmake -B "%DM_OUT%" --graphviz="%DM_GRAPH%" && dot -Tpdf "%DM_GRAPH%" -o "%DM_GPDF%" && explorer.exe /select,"%DM_GPDF%"

