@echo off

if not exist "%DM_OUT%\CMakeCache.txt" (
    echo no cmake config found! exiting...
    exit /b -1
)

cmake --build "%DM_OUT%" %*

