@echo off

rem параметры: "tag1 tag2...tagN" "строка ввода с неизвестным набором тегов"
rem Выделяет из строки ввода первый найденный тег без учёта регистра и помещает
rem его нормальную форму в переменную CASE, возвращает нулевой код успеха.

setlocal enabledelayedexpansion
if ":%~2%~3%~4%~5%~6%~7%~8%~9" == ":" exit /b 1
set "CA=%~1 "
set "CA=%CA:     = %"
set "CA=%CA:    = %"
set "CA=%CA:   = %"
set "CA=%CA:  = %"
set "CA=%CA:~0,-1%"
set "CA=echo %CA: =&echo %"
set "ESCP="
for %%i in (%~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9) do set "ESCP=!ESCP! \^<%%i\^>"
set "ESCP=%ESCP:~1%"
set "CASE="
for /F "tokens=* usebackq" %%i in (`@cmd /c "%CA%" ^| findstr /I /R "%ESCP%"`) do (
    if ":!CASE!" == ":" set "CASE=%%i"
)
if ":!CASE!" == ":" exit /b 2
endlocal & set "CASE=%CASE: =%"
exit /b 0

