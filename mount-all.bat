@echo off
pushd "%~dp0"

call :mctrl "file_a.zip"
call :mctrl "file_b.zip"
call :mctrl "file_c.zip"

goto eof

:mctrl
    if not exist "%~1/" (
        pfm mount -a -s -r -c -l -f PFMZIPFS "%~1"
        exit /b 0
    )
    pfm unmount "%~1"
    exit /b 0

:eof
popd

